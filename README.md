# Count word occurrence in file
## Description
Service helps to count word's occurrences in present file

## Technical details

- [Source code](https://gitlab.com/kovalexeyko/simcorpjava) in GitLab.

## PREREQUISITES

* setup 11 Java
* setup latest Maven (for case with running the test from IDE)
* prepare file with words separated by space

## How to run

There are few ways to run the program:
* run from IDE main class
* run program from command line and predefined file in parameters:
```
java -jar testSimCorp-1.0.jar test.txt
```
* to run program with entering file name after running:
```
java -jar testSimCorp-1.0.jar
```
## Result
Expected result for string "Go do that thing that you do so well" will be:
```
1: Go
2: do
2: that
1: thing
1: you
1: so
1: well
```
