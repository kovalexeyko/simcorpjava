package org.example;

import org.junit.jupiter.api.Test;

import static org.example.Example.wordsOccurrence;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExampleTest {
    @Test
    public void checkEmptyResultForEmptyString() {
        assertEquals(0, wordsOccurrence("").size());
    }

    @Test
    public void checkEmptyResultForStringWithSpaces() {
        assertEquals(0, wordsOccurrence("   ").size());
    }

    @Test
    public void checkOnlyOneWord() {
        assertEquals(1, wordsOccurrence("word").get("word"));
    }

    @Test
    public void checkSeveralOccurrenceForWordOnly() {
        assertEquals(2, wordsOccurrence("word word").get("word"));
    }

    @Test
    public void checkSeveralOccurrenceForWordWithOtherWords() {
        assertEquals(2, wordsOccurrence("word hello word").get("word"));
    }

    @Test
    public void checkSeveralOccurrenceForWordWithOtherWordsInDifferentCases() {
        assertEquals(2, wordsOccurrence("word HELLO WORD").get("word"));
    }
}
