package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Example {

    public static void main(String[] args) {
        String fileName = "";

        if (args.length == 0) {
            System.out.println("You don't provide a file in arguments, so type the file name (should be in current folder):");
            var scanner = new Scanner(System.in);
            fileName = scanner.nextLine();
        } else {
            fileName = args[0];
        }

        Path filePath = Path.of(fileName);
        String text = null;
        try {
            text = getStringFromFilePath(filePath);
        } catch (IOException e) {
            System.out.println("File not found, try again later");
            return;
        }

        var map = wordsOccurrence(text);

        map.entrySet().stream().forEach(entry -> {
            System.out.println(entry.getValue() + ": " + entry.getKey());
        });
    }

    public static LinkedHashMap<String, Integer> wordsOccurrence(String text) {
        var map = new LinkedHashMap<String, Integer>();
        if (text.replaceAll(" ", "").isEmpty()) {
            return map;
        }
        var wordsArray = text.split(" ");
        for (String word : Arrays.stream(wordsArray).map(word -> word.toLowerCase()).collect(Collectors.toList())) {
            if (!map.containsKey(word)) {
                map.put(word, 1);
            } else {
                var score = map.get(word);
                map.put(word, score + 1);
            }
        }
        return map;
    }

    private static String getStringFromFilePath(Path filePath) throws IOException {
        var fileBufferReader = Files.newBufferedReader(filePath);
        var content = fileBufferReader.lines().map(Object::toString)
                .collect(Collectors.joining(" "));
        return content;
    }
}